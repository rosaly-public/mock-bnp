FROM python:3.8-alpine
COPY mock mock/
COPY ssl ssl/
ENV  MOCK_BNP_PORT 8089
EXPOSE 8089
# -u is for unbuffered output
# otherwise you will not directly see the stacktrace in the logs when an error occur
# having it unbuffered ease debugging
CMD ["python", "-u", "-m" , "mock"]
