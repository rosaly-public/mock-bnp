# mock BNP

run in autload moad with 

```
ls mock/*.py | entr -r python -m mock
```

## Change behaviour

By default all IBAN will be considered as reachable through SCT Inst. All payments will be considered as accepted

If you want to change this you can call `MOCK_CHANGE_BEHAVIOUR` http method, on any url (does not matter), with the following payload

```
{
    'is_reachable': true,
    'default_payment_status': 'ACCP',
}
```

(you can change the values as you like, and only keys you provide will be overrided)

you can reset to default behaviour by using the http method `MOCK_FLUSH`

## Env variables

To connect to it you need a two way SSL (https://cheapsslsecurity.com/p/what-is-2-way-ssl-and-how-does-it-work/)

On your **client** you will need the following certificate

```
-----BEGIN CERTIFICATE-----
MIIDazCCAlOgAwIBAgIUTR0Xyvo+snare8yJfptczrWJyYwwDQYJKoZIhvcNAQEL
BQAwRTELMAkGA1UEBhMCQVUxEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMzA2MTIyMTQ1NTNaFw0zMzA2
MDkyMTQ1NTNaMEUxCzAJBgNVBAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEw
HwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwggEiMA0GCSqGSIb3DQEB
AQUAA4IBDwAwggEKAoIBAQCoYHoTNfSopSC0nAtdlozSxBe5aWImPDHkOPvJYRiR
LE4t/nml6xa4JdvvDa285rKzl4PXt4j1NTQHF7aAmy1UF6pu7JgrtxZ83SLp9ES4
e6i3l7gbXs4IJLBiSLwtbtQtiaydByPDfMIlRBWJ+SSyXLyBxvaADLAuqtK0mwVD
1265+vJ1Vqh1Hdyko+YoPx054ENSzeTAmji8aB6CA0Q5cuZozqr8upDpMZOL0yjI
bjGeM8T3WRIIK7plbgy/ZuJh2TDOpf6QUFrpnHWScnN30ou//g29k/H8hjAJQOLL
9cMQoE7j8+/uHCIyv1Ci0oprNwCmz3jIBHWZJ+sUynWNAgMBAAGjUzBRMB0GA1Ud
DgQWBBS1qomN2120AwfJB3OZusRNJSVCQTAfBgNVHSMEGDAWgBS1qomN2120AwfJ
B3OZusRNJSVCQTAPBgNVHRMBAf8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4IBAQBk
e9RpLCXWQLkt1S4aMm7t55otFyqjvoHX+aXSjEFmglRva3n3UorEyJwRM8gdpzlk
n7diHJ7G824HPZyUcKkQohZWVtpY7XwC7Ald3LEG6CNmyNgOpFsMvCPMh74jk6Cf
NqJyyj93ocea0PhjFq1NR/MjQE5+UqtqhtMBN5prRJtNBpJMrT0AYLLpPHKGoUXD
9UPsfweyI6SW/akNJWK1hKLuwkEqq7RoyMRVHZfPvEGehxNyjCZTgt4xY1sMaroU
9cvQ2Z2m3K7Vp93t6byh8a6r6oSIWyDD09iKci5F5Mw5gqcUWUVbgyUxoSDVl3/G
gpIblv2d1enemzc9WNx/
-----END CERTIFICATE-----
```

and the following private key

```
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCoYHoTNfSopSC0
nAtdlozSxBe5aWImPDHkOPvJYRiRLE4t/nml6xa4JdvvDa285rKzl4PXt4j1NTQH
F7aAmy1UF6pu7JgrtxZ83SLp9ES4e6i3l7gbXs4IJLBiSLwtbtQtiaydByPDfMIl
RBWJ+SSyXLyBxvaADLAuqtK0mwVD1265+vJ1Vqh1Hdyko+YoPx054ENSzeTAmji8
aB6CA0Q5cuZozqr8upDpMZOL0yjIbjGeM8T3WRIIK7plbgy/ZuJh2TDOpf6QUFrp
nHWScnN30ou//g29k/H8hjAJQOLL9cMQoE7j8+/uHCIyv1Ci0oprNwCmz3jIBHWZ
J+sUynWNAgMBAAECggEAEx5dTigevzV8yLU9PyO0mswES5vRNrk4CwuDX4LkaLuI
pwVRp8rRWhiGaBb8BsqFOQo3wLtI1Fr1rOZ+5Qx5saY8Rnsb7j7MUWJEsB753J7B
8hLKp2T07QzJcGeE4n3GnuMWk//3Ch/05YldDr0Y8x2/k9bVL4YBTVM9OJ+vkdvJ
rWS/OZLLpRX7iOfGSybBc2sGdN++KcRwoZ3ZNk3g/hU5f/e6nMPC/DmHyLVpBXeX
0q30tzP6YfEBmwILc7vvKMbp+7D8/yPB2GsUnfchk4UY/rPrTkBVFHZgruLMEKqe
QrucxURKtyNtlU0WkIYAyYMz1rkBEh5nfAb2TDQlMQKBgQDcfBw8IDvGWLYcrKco
5AdhQLQ9F7VZjVhduRYorKgbwjFJ6eCstafo84PwUqEC5zGNs1mKlu1yCRHBVraE
cwmGqBMn66mvD2sa6F5mwgd4b/ZA4iXwrVir2RAv/3QsxwwC5gH9Kwp2Hq/Rd/jW
ADAP4d+GjPkIx3lwUi9LqYVrJQKBgQDDf6YICuZg+J+HLOdRQ9Zs9sPB2AMgn55z
eq1sSlGRJsQg3eegT0gxlYIEFx1Gg5dzYmxNFe8jvY2LLZZWD/ErMeJsoXOOtlAE
H2pMFPjMzo79y+gcKcC5Th7WqUTt5+xih7+ER5ukhbZtFfxFSqrT3CEyPQk0SzZ1
MC6SEMLISQKBgGhjCisdR6+ciHwC8hFWBHhy3eKeuulvFgspeeg/9kdOJW/NHuYm
Cx0rTagWNszdEIT6CMaotjprONDEcR08KaJrKMaZuHh9YEQZB7i23DqVJ42IhFXy
fCzAU/2lgCdDBdjJdN+b5RQPa0ZM+q624Apk8iMN00noxwH6pxG3bMdRAoGAavHK
Vr33tvZq9lxnEfqFM0huX/0WAOOwn0Vt1z50jm3hfoVFr1mPiaha3nfyiN91myYt
TqIAFtIbuxYuFlhUOqfoUDql51zuOsMguAwE6qzYZhgEmltw1S3aCt3AGlDz7BaM
ewVdLEwFNnl/bjxhZNIXys6+2T+Hw+WepELM+xkCgYEAl3To2KusOS6wK2TxXIdM
B6V1hjTrNwci/RiQhuc4pYueFkefCA2RqwVw5jkNxj2hSX/c+V0Z8Lnlf8XzszWG
yhAe4Csh7Wfs8whODdjjkD1OZckZA4jzJagL3Y/KblX7/6RC+lNIID51ZeEXv2yx
PxANcpAZRsckmJaCKV1sUWs=
-----END PRIVATE KEY-----
```


⚠️ ⚠️  On your client you will also need to deactivate the SSL verification as the server has a self signed certificate ⚠️ ⚠️ 

On the server you will need to declare the following env variables

  * `MOCK_BNP_APP_ID`
  * `MOCK_BNP_APP_SECRET`


**Optionally** you can declare `MOCK_BNP_PORT` to change the port on which it listen
