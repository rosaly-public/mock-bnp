"""
Launch a fake BNP SCT Inst API
"""

import os
import ssl
from http.server import HTTPServer

from .handler import MockHandler


def launch_server(hostname, port_number):
    """
    Start a mock instance of BNP listening
    on given hostname and port
    """
    httpd = HTTPServer((hostname, port_number), MockHandler)
    # ⚠️  important part, here we wrap the HTTP connection
    # to make it a two-way SSL connection
    httpd.socket = ssl.wrap_socket(
        httpd.socket,
        server_side=True,
        certfile="ssl/server.crt",

        keyfile="ssl/server.key",
        ca_certs="ssl/client.crt"
    )
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()


if __name__ == "__main__":
    PORT = int(os.getenv("MOCK_BNP_PORT", "8089"))
    print("starting service on ", PORT)
    launch_server("0.0.0.0", PORT)
