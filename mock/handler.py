# Disabled because pylint does not like upper case do_GET
# but we have to do so, otherwise it's not mapped to HTTP
# verb GET etc.
#
# pylint: disable=invalid-name
from typing import Union
from typing import Optional
from typing import List
from typing import Dict

import base64
import json as json_module
from urllib.parse import parse_qsl
import os
import uuid
from pathlib import Path
from string import Template

from http.server import BaseHTTPRequestHandler

CLIENT_ID = os.environ["MOCK_BNP_APP_ID"]
CLIENT_SECRET = os.environ["MOCK_BNP_APP_SECRET"]

DATA_STORE: Dict = {}
PAYMENTS_STORE: Dict = {}
ACCESS_TOKENS: Dict = {}
BEHAVIOUR_OPTIONS: Dict = {
    'is_reachable': True,
    'default_payment_status': 'ACCP',
}

class MockHandler(BaseHTTPRequestHandler):
    """ Handle all requests as if it was a Linxo instance
    """


    def do_GET(self):
        """Answer to GET methods
        """
        #
        # get the status of one payment
        #
        if self.path.startswith('/credit-transfer/sct-inst/V1.0/instant-payment-requests/'):
            # the id is not the last part of the path but the one before
            # hence the -2
            transaction_id = self.path.split('/')[-2]

            transaction = PAYMENTS_STORE.get(transaction_id)

            if transaction is None:
                # TODO verify the actual response in that case
                self._send_json_response(
                    {
                    },
                    status_code=404,
                )
                return

            self._send_json_response(
                {
                    'originalPaymentInformationAndStatus': {
                        'paymentInformationStatus': BEHAVIOUR_OPTIONS['default_payment_status'],
                    },
                    'groupHeader': {
                        # TODO: simulate the fact that actually the id returned here
                        # may not be the one requested initially
                        'messageIdentification': transaction_id,
                    }
                },
                status_code=200,
            )
            return


        if self.path == '/' or self.path.startswith('/?'):
            with Path(__file__).with_name('debug.html').open('r') as f:
                # we use the debug.html file as a template/twig file
                template = Template(f.read())
                body = template.substitute({
                    'behaviour': json_module.dumps(BEHAVIOUR_OPTIONS, indent=4),
                    'toggle_reachability': json_module.dumps(BEHAVIOUR_OPTIONS['is_reachable']),
                    'default_payment_status': BEHAVIOUR_OPTIONS['default_payment_status'],
                    'data': json_module.dumps(PAYMENTS_STORE, indent=4),
                })
                self._send_raw_html_response(body.encode('utf-8'))
            return

        self._send_json_response(
            json={},
            status_code=404,
        )

    def do_POST(self):
        """Handle POST requests
        """
        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)


        #
        # Check if the IBAN can be reached by SCT Inst
        #
        if self.path.startswith('/credit-transfer/sct-inst/V1.0/instant-payment-reachability'):
            if self._verify_auth() is None:
                self._send_json_response(json={}, status_code=401)
                return

            self._send_json_response(
                {
                    'ibanDebtor': '',
                    'bicDebtor': '',
                    'ibanCreditor': '',
                    'bicCreditor': '',
                    # the important field is this one
                    'reachable': BEHAVIOUR_OPTIONS['is_reachable'],
                },
                status_code=200,
            )

            return

        #
        # Create a payment requests
        #
        if self.path.startswith('/credit-transfer/sct-inst/V1.0/instant-payment-requests'):
            if self._verify_auth() is None:
                self._send_json_response(json={}, status_code=401)
                return

            data_dict = json_module.loads(body_string)
            print(data_dict)

            bnpTransactionId = 'bnpId' + str(uuid.uuid4()).replace('-', '')
            PAYMENTS_STORE[bnpTransactionId] = {}

            self._send_json_response(
                {
                    'requestTransactionId': bnpTransactionId
                },
                status_code=200,
            )

            return

        #
        # Used for token authentification
        #

        if self.path.startswith('/auth/oauth2/token'):
            body_dict = dict(parse_qsl(body_string.decode('utf-8')))

            # used for Admin actions (create users etc.)
            if body_dict.get('grant_type') == 'client_credentials':

                authorization_header = self.headers.get("Authorization")
                client_id_and_client_secret = base64.decodebytes(
                    authorization_header[len("Basic ") :].encode()
                ).decode()
                client_id, client_secret = client_id_and_client_secret.split(":")
                print(client_id_and_client_secret)

                if client_id != CLIENT_ID or client_secret != CLIENT_SECRET:
                    self._send_json_response(json={}, status_code=403)
                    return

                access_token = 'mock_admin_access_token_' + str(uuid.uuid4())
                ACCESS_TOKENS[access_token] = {}

                self._send_json_response({
                    'access_token': access_token,
                    'expires_in': 600,
                })
                return


            # if it's not a grant type covered -> 403

            self._send_json_response(json={}, status_code=403)
            return

        self._send_json_response(
            json_body={},
            status_code=404,
        )

    def _verify_auth(self) -> Optional[Dict]:
        authorization_header = self.headers.get("Authorization")
        if not authorization_header.startswith('Bearer '):
            return False

        # we strip 'Bearer ' from the header to get the token
        access_token = authorization_header[len('Bearer '):]
        return ACCESS_TOKENS.get(access_token)

    #
    #  MOCK specific-part
    #
    #
    def do_MOCK_CHANGE_BEHAVIOUR(self):
        """ Handle non-standard HTTP verb 'MOCK_CHANGE_BEHAVIOUR',
        to change the behaviour of the API, useful when you want
        to test what happens in this or that case
        """

        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)

        behaviour_options = json_module.loads(body_string)


        BEHAVIOUR_OPTIONS.update(behaviour_options)

        self.send_response(204)
        self.end_headers()


    def do_MOCK_INJECT(self):
        """ Handle non-standard HTTP verb 'MOCK_INJECT', to inject in cache

        The reason of using a non-standard verb is that we're sure
        not to overlap with existing API mapping

        It's used to prepopulate the given URL with the given data, so that
        the next GET requests on the very same URL (including get parameter)
        will get the given data in body
        """
        body_size = int(self.headers["Content-Length"])
        body_string = self.rfile.read(body_size)

        # allow the client to precise what will the status code
        # of its injected response
        # NOTE: if needed the same logic can be extended to other headers
        # as well
        status_code = int(self.headers.get("X-Mock-Status-Code", "200"))

        DATA_STORE[self.path] = {"status_code": status_code, "body": body_string}

        self.send_response(204)
        self.end_headers()

    def do_MOCK_FLUSH(self):
        """ Handle non-standard HTTP verb 'MOCK_FLUSH', to empty internal cache

        The reason of using a non-standard verb is that we're sure
        not to overlap with existing API mapping

        the URL given with this method is currently not used
        """

        DATA_STORE.clear()
        PAYMENTS_STORE.clear()
        global BEHAVIOUR_OPTIONS
        BEHAVIOUR_OPTIONS = {
            'is_reachable': True,
            'default_payment_status': 'ACCP',
        }

        self.send_response(204)
        self.end_headers()


    def do_MOCK_DEBUG(self):
        """ Handle non-standard HTTP verb 'MOCK_DEBUG', to output DATASTORE

        the URL given with this method is currently not used
        """

        self.send_response(200)
        self.send_header("Content-Type", "application/json")
        self.end_headers()

        self.wfile.write(json_module.dumps(DATA_STORE))
        self.wfile.close()


    def _send_json_response(self, json: Union[Dict, List], status_code=200, headers={}):
        self._send_raw_json_response(
            json_body=json_module.dumps(json).encode("utf-8"),
            status_code=status_code,
            headers=headers
        )

    def _send_raw_json_response(self, json_body: bytes, status_code=200, headers={}):
        self.send_response(status_code)
        self.send_header("Content-Type", "application/json")
        for name, value in headers.items():
            self.send_header(name, value)
        self.end_headers()
        self.wfile.write(json_body)

    def _send_raw_html_response(self, html_body: bytes, status_code=200, headers={}):
        self.send_response(status_code)
        self.send_header("Content-Type", "text/html")
        for name, value in headers.items():
            self.send_header(name, value)
        self.end_headers()
        self.wfile.write(html_body)

    def _send_blank_response(self, status_code):
        self.send_response(status_code)
        self.end_headers()
